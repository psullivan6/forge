# Forge

# Notes
- why `forge`? Well yarn already took `create`, so we needed something else and it was the coolest sounding synonym

# TO-DO
- add `promotion` flag or similar to track forged files and allow for easy cleanup later
  - might not be worth it, especially if files are created outside this `forge` utility script
-


## Goals

- expose ability to call `forge` directly in the `scripts` section of the importing repo, like:
```
scripts: {
  "forge": "forge --templateDir=/custom/templates/path"
}
```
- use inquirer when arguments aren't filled out


## Templates

### Alternates

**Styled Components**

Full Component import with `.` syntax reference
```
`import FullComponent from './styles'`
...
<FullComponent.Container>
  <FullComponent.Headline>Headline Here</FullComponent.Headline>
</FullComponent.Container>
```




### Commands

`yarn forge Cool`
- ask what type

`yarn forge class-component Cool`
- just forge it

`yarn forge utility Cool`
- just forge it

`yarn forge component Cool`
- forge with default `component` setting



### Component Types

See React docs

- functional
- class
- pure

