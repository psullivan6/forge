- [ ] document when to use each `component` option
- [x] get executable working in external repo
- [ ] get the arguments documented and passing through correctly
- [ ] write the templates
- [ ] write the template string replacer
- [ ] leverage `@babel/register` or similar to use ES6 `import` syntax
  - need to compile files on publish
  - leverage `.npmignore` if published files different from `.gitignore`


- `fs` READ off the `templates` directory to get the full list of types
- allow a `.forgeconfig` file within each template type directory to override any settings
- allow `ForgeReplace` string to be a setting




## Things to read
- https://overreacted.io/writing-resilient-components/
- https://overreacted.io/how-are-function-components-different-from-classes/
- https://github.com/zeit/pkg
-