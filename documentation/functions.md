Misc. documentation about functions and how to write the function-component type

**Function declarations**
- `const foo = () => {};`
- load before any code is executed

**Function expressions**
- `function foo() {}`
- load only when the interpreter reaches that line of code.



A closure is the combination of a function and the lexical environment within which that function was declared.