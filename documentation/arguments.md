Command:

`yarn forge [TYPE] [NAME]`

## Type

### Options

- `utility`
- `component`
  - `function-component` - default
  - `class-component`
  - `pure-component`
- `service`


## Name

### Requirements

- UpperCamelCase
- No special characters




---

# Misc.

## Arguments

## Flags

`templatesDir`: manually assign a directory for template files
`type`: this is implied and no `--` flag required, perhaps
`outputDir`: manually assign an output directory, defaults to directory where the command is run

### Types

- `Utility`
- `Component`
- `Service`
-
