# Misc. Notes:

- Settings to make a node executable: https://docs.npmjs.com/files/package.json#bin
- Use this for building a man file (if needed, README should suffice) - https://github.com/rtomayko/ronn#readme


# Testing

https://medium.com/@the1mills/how-to-test-your-npm-module-without-publishing-it-every-5-minutes-1c4cb4b369be



# Work Steps:

- get executable working in external repo
- get the arguments documented and passing through correctly
- write the templates
- write the template string replacer
- leverage `@babel/register` or similar to use ES6 `import` syntax
  - need to compile files on publish
  - leverage `.npmignore` if published files different from `.gitignore`
-