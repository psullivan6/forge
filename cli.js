#!/usr/bin/env node

const { type, name, templatesDir, outputDir } = require('./bin/utilities/getArguments')(__dirname);
const getTemplates = require('./bin/utilities/getTemplates');

console.log('type', type);
console.log('name', name);
console.log('templatesDir', templatesDir);
console.log('outputDir', outputDir);

console.log('TEMPLATE DIRECTORIES', getTemplates(templatesDir));