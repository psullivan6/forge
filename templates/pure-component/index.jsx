import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

// Styles
import { Container } from './styles';


class ForgeReplace extends PureComponent {
  componentDidMount() {
    console.log('ForgeReplace - componentDidMount', 'go GET the data');
  }

  render() {
    const { TEST } = this.props;

    return (
      <Container>
        <h1>ForgeReplace</h1>
        <h2>{`TEST prop value: ${TEST}`}</h2>
      </Container>
    )
  }
}

ForgeReplace.defaultProps = {
  TEST: 'default value',
};

ForgeReplace.propTypes = {
  TEST: PropTypes.string,
};

export default ForgeReplace;