import styled from 'styled-components';

export const Container = styled.section`
  margin: 0;
  padding: 1rem;
  background-color: #333;
  color: white;
`;

export const Headline = styled.h1`
  margin: 0 0 1em 0;
  text-transform: uppercase;
`;

export const Copy = styled.p`
  margin: 0;
  line-height: 1.2;
`;

export default {
  Container,
  Copy,
  Headline
};
}