import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Styles
import {
  Container,
  Copy,
  Headline,
} from './styles';


class ForgeReplace extends Component {
  componentDidMount() {
    console.log('ForgeReplace - componentDidMount');
  }

  handleClick = () => {
    console.log('BUTTON was clicked');
  }

  render() {
    const { TEST } = this.props;

    return (
      <Container>
        <Headline>ForgeReplace</Headline>
        <Copy>{`TEST prop value: ${TEST}`}</Copy>
        <button onClick={this.handleClick}>CLICK MEH</button>
      </Container>
    )
  }
}

ForgeReplace.defaultProps = {
  TEST: 'default value',
};

ForgeReplace.propTypes = {
  TEST: PropTypes.string,
};

export default ForgeReplace;