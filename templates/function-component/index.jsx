import React from 'react';
import PropTypes from 'prop-types';

// Styles
import {
  Container,
  Copy,
  Headline,
} from './styles';


const ForgeReplace = ({ TEST }) => {
  const handleClick = () => {
    console.log('BUTTON was clicked');
  }

  return (
    <Container>
      <Headline>ForgeReplace</Headline>
      <Copy>{`TEST prop value: ${TEST}`}</Copy>
      <button onClick={handleClick}>CLICK MEH</button>
    </Container>
  );
}

ForgeReplace.defaultProps = {
  TEST: 'default value',
};

ForgeReplace.propTypes = {
  TEST: PropTypes.string,
};

export default ForgeReplace;