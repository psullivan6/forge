import React from 'react';
import PropTypes from 'prop-types';

// Styles
import {
  Container,
  Copy,
  Headline,
} from './styles';


const ForgeReplace = ({ TEST }) => (
  <Container>
    <Headline>ForgeReplace</Headline>
    <Copy>{`TEST prop value: ${TEST}`}</Copy>
  </Container>
)

ForgeReplace.defaultProps = {
  TEST: 'default value',
};

ForgeReplace.propTypes = {
  TEST: PropTypes.string,
};

export default ForgeReplace;