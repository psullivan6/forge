const { lstatSync, readdirSync } = require('fs')
const { join } = require('path')

const getTemplates = (templateDirectory) => {
  const isDirectory = directory => lstatSync(directory).isDirectory()
  const directories = readdirSync(templateDirectory)
    .filter(templatePath => isDirectory(join(templateDirectory, templatePath)));
  console.log('directories', directories);
}

module.exports = getTemplates;
